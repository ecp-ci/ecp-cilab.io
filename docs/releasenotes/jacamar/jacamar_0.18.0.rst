Jacamar CI v0.18.0
==================

* *Release*: `v0.18.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.18.0>`_
* *Date*: 3/7/2024

.. note::

  Changes in this release are required in order to function with
  GitLab server *17.0+*. Please make sure to upgrade Jacamar CI before
  deploying this
  `upcoming server release <https://about.gitlab.com/releases/>`_`
  to avoid downtime.

Admin Changes
-------------

* Migrate JWKS to ``/oauth/discovery/key`` endpoint
  (`!465 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/465>`_)

  - This makes GitLab server *16.0+* a requirement for Jacamar CI.

* Deprecation warning for ``job_token_scope_enforced`` configuration
  (`!470 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/470>`_)

  - Changes in GitLab server *17.0+* will break this functionality. Please
    migrate away from using it before upgrading your GitLab instance.

* Clearer error message for JWKS endpoint requests
  (`!474 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/474>`_)

  - .. code-block:: shell

        Unable to retrieve key from JWKS endpoint: unable to complete http request: Get "https://127.0.0.1/oauth/discovery/keys"

* Configure option to allow users to request Bash shell with ``--noprofile``
  (`!475 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/475>`_)

  - .. code-block:: toml

      [general]
        optional_login_shell = true

  - Setting this allows users to define ``JACAMAR_NO_BASH_PROFILE: 1`` for
    their job to use a shell created with ``--noprofile`` to run the GitLab
    generated job scripts.

  - We highly advise testing this feature with your local environment and
    create issues if other options/configuration will help from an
    administrative standpoint to offer this sort of option. We cannot help
    with troubleshooting users issues they may experience by using this feature
    caused by missing environment variables (e.g., unable to submit job due to
    missing application on default ``PATH``).

Bug & Development Fixes
-----------------------

* Vendor and update all dependencies
  (`!471 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/471>`_,
  `!473 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/473>`_)
* Upgraded to Go version *1.21.8*
  (`!478 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/478>`_)
* Update runner integration testing
  (`!466 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/466>`_)
