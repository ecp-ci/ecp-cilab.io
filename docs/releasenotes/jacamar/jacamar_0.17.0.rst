Jacamar CI v0.17.0
==================

* *Release*: `v0.17.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.17.0>`_
* *Date*: 10/27/2023

Admin Changes
-------------

* Drop ``flux mini alloc`` command for newer ``flux alloc``
  - (Thanks ``@alecbcs``)
  (`!499 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/449>`_)
* Optional run sub-stage allowlist
  (`!450 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/450>`_)

  - If configured only stages that are listed will be allowed. This
    is envisioned for cases where a runner is only meant to allow
    arbitrary user scripts to run on compute resources.

  - .. code-block:: toml

        [auth]
        run_stage_allowlist = [
            "prepare_script",
            "get_sources",
            "download_artifacts",
            "step_script",
            "upload_artifacts_on_success",
            "upload_artifacts_on_failure"
        ]

  - See the `Custom Executor <https://docs.gitlab.com/runner/executors/custom.html#run>`_
    documentation for the list of potential stages.

User Changes
------------

* Add support for ``scancel`` signal variable
  (`!462 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/462>`_)

  - Currently only support for Slurm
    (`--signal <https://slurm.schedmd.com/scancel.html>`_), defining the
    ``SCHEDULER_CANCEL_SIGNAL`` job variable will result in the
    provided type being used if a job needs to be terminated.

  - .. code-block:: yaml

      job:
        variables:
          SCHEDULER_CANCEL_SIGNAL: "KILL"

Bug & Development Fixes
-----------------------

* Simplify and update the augmenter package and expand testing
  (`!448 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/448>`_)

  - This removed a number of elements of the packages focused on
    aspects of Jacamar CI that are no longer supported. It has also
    added tests to the nightly run that ensures modifications to the
    artifact scripts and addition of ``GIT_ASKPASS`` work as
    expected.

* Upgraded to Go version *1.20.10*
  (`!460 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/460>`_)
* Expanded testing for RunAs identities
  (`!452 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/452>`_)
* Multiple updates to supported containers
  (`!455 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/455>`_,
  `!456 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/456>`_)
* General updates to multiple dependencies
  (`!454 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/454>`_)
