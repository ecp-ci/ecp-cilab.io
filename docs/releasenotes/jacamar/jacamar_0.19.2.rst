Jacamar CI v0.19.2
==================

.. _Deploying and Using the Podman Run Mechanism: ../../guides/deploy-use-podman-mechanism.html

* *Release*: `v0.19.2 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.19.2>`_
* *Date*: 5/9/2024

.. note::

  This patch only contains fixes for the supported Podman run mechanism.
  Please see the `Deploying and Using the Podman Run Mechanism`_ guide
  for complete details.

User Changes
------------

* Support user defined hostname for Podman via ``JACAMAR_CI_HOSTNAME`` variable
  (`!494 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/494>`_)

  - Providing the ``$HOSTNAME`` variable is a special case and will resolve
    to the hostname of the system where the ``podman run ...`` run command
    is executed. In the case of a batch executor (e.g., Slurm/Flux) this
    should be the compute resource.

  - .. code-block:: yaml

      job:
        variables:
          JACAMAR_CI_HOSTNAME: $$HOSTNAME
        script:
          - make test

Admin Changes
-------------

* Improve user volume identification and handling for container mechanisms
  (`!496 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/496>`_)

  - Incorrectly formatted environment variables that will not be accepted
    should be better identified via warning messages to the user.

* Add optional `volume_labels` for Podman run mechanism
  (`!492 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/492>`_)

  - This will fix issues where we incorrectly used labels (`:z` and `:Z`)
    in deployments they were not required. This behavior will now be optional.

  - .. code-block:: toml

      [general.podman]
        volume_labels = true
