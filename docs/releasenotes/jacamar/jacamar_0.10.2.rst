Jacamar CI v0.10.2
==================

* *Release*: `v0.10.2 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.10.2>`_
* *Date*: 3/9/2022

.. note::

    Release *v0.9.0* relocated all RPM installed binaries into a single
    location, ``/opt/jacamar/bin``. This will offer a better standard moving
    forward, please be aware of this when upgrading from an older version.

Admin Changes
-------------

* Add ``gitlab-runner`` group for RPM w/capabilities to address
  missing GID in select cases
  (`!328 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/328>`_,
  `!329 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/329>`_)

  - This enforces the expected behaviour from the
    `official RPM <https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/packaging/root/usr/share/gitlab-runner/post-install#L20>`_
    in cases where a group was not created on the system.
