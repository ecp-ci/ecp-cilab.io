Jacamar CI v0.23.0
==================

.. _configuration documentation: ../../admin/jacamar/configuration.html#limit-build-dir
.. _Deploying and Using Scheduler Actions: ../../guides/scheduler-actions.html

* *Release*: `v0.23.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.23.0>`_
* *Date*: 10/16/2024

User Changes
------------

* Dynamic ``SCHEDULER_PARAMETERS`` via script
  (`!535 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/535>`_)

  - This feature is now available for all users of batch executors (e.g.,
    Flux, Slurm, PBS, and Cobalt). It enables scripts to be included in your
    repository and referenced by the ``SCHEDULER_PARAMETERS`` to drive
    specifics of the arguments. For example:

    .. code-block:: shell

        #!/bin/bash
        # Additional logic/checks...
        echo '{"args":"-N1 -A stf040 -t 00:30:00"}'

    You would include this ``args.sh`` file in your repository and update
    your CI job like this:

    .. code-block:: yaml

        job:
          variables:
            SCHEDULER_PARAMETERS: args.sh
          script:
            - make test

    The userspace Jacamar application will detect this proposed
    parameter is a file and execute it, parsing the returned
    output for an acceptable JSON structure.

    It is also possible to simply skip the job, useful if you wish to
    end a job successfully if a scheduler is too busy to complete your
    job within an acceptable time frame:

    .. code-block:: shell

        #!/bin/bash
        # Additional logic/checks...
        echo '{"skip":true}'

  - Please note this script has several limitations including the duration
    it can run (90 seconds). It is only meant to preform minimal logic and
    return details via `stdout`. If more comprehensive operations are required
    we advise doing so in an earlier job and providing the
    necessary details via an artifact.

  - Runner administrators can disable this with the ``batch.disable_user_args``
    configurations.

Admin Changes
-------------

* Improved support for ``limit_build_dir`` configuration
  (`!533 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/533>`_)

  - The goal with this feature it to allow for the use of file locking
    (`fcntl <https://www.man7.org/linux/man-pages/man2/fcntl.2.html>`_) to
    claim build directories within the need to runner specific identification.
    This is useful for deployment with a large number of runners all
    configured to utilize the same ``data_dir``.

  - Complete details can be found in the `configuration documentation`_

* Optional user driven ``SCHEDULER_ACTIONS`` for Slurm
  (`!513 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/513>`_)

  - The `Deploying and Using Scheduler Actions`_ guide has
    complete details. We are excited to hear if this new feature
    can be value for your deployments.

Bug & Development Fixes
-----------------------

* Added functions for extra context messages in errors
  (`!527 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/527>`_)
* Go version *1.22.7*
  (`!530 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/530>`_)
* Flux image version *0.64*
  (`!529 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/529>`_)
* Expand ``runmechanisms.StepScriptStage`` usage
  (`!531 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/531>`_)
* Minor correction to fix quality errors
  (`!528 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/528>`_,
  `!526 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/526>`_)
* Spack environment support for development
  (`!536 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/536>`_)
