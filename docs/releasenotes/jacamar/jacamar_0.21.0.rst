Jacamar CI v0.21.0
==================

* *Release*: `v0.21.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.21.0>`_
* *Date*: 7/9/2024

Admin Changes
-------------

* Introduce optional Podman arg variable
  (`!503 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/503>`_)

  - .. code-block:: yaml

      variables:
        JACAMAR_CI_PODMAN_ARGS: "--workdir /example"

  - This can be disabled with the `disable_user_args` configuration.

* Support setting stack size in userspace
  (`!501 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/501>`_)

  - In RHEL 8 (see:
    `https://access.redhat.com/solutions/6454591 <https://access.redhat.com/solutions/6454591>`_)
    when using Jacamar CI w/capabilities the stack size limits are not
    inherited, instead they default to ``8M``. This change optional
    configuration sets the limit and is the equivalent of
    of user running a ``ulimit -s`` command.

  - .. code-block:: toml

      [general]
        set_stack_size = true

* Improve scheduler args and support default configurations
  (`!505 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/505>`_)

  - .. code-block:: toml

      [batch]
        default_args = ["--clusters=example"]

  - DefaultArgs (``default_args``) provides an admin controlled mechanism to
    inject arguments into the job submission commands.

User Changes
------------

* Print Podman run command to job log
  (`!504 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/504>`_)

Bug & Development Fixes
-----------------------

* Correct Podman ``StepScriptOnly`` logic
  (`!502 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/502>`_)
* Go version *1.22.4*
  (`!508 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/508>`_)
* Update dependencies
  (`!507 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/507>`_)
* Add Pipeline ID to stateful environment
  (`!506 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/506>`_)
