Jacamar CI v0.20.0
==================

* *Release*: `v0.20.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.20.0>`_
* *Date*: 5/9/2024

Admin Changes
-------------

* Improve performance and logging of limited_build_dir feature -
  (Thanks ``@ahmedfahmy1`` for reporting/testing)
  (`!458 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/458>`_)

  - This effort seeks to overhaul the ``ff_limit_build_dir`` option and
    improve performance across more systems. In addition, debug logging has
    been introduced, these files can be generated in the projects builds
    directory and can help with troubleshooting.

  - .. code-block:: toml

      // FileLockDebug if enabled will create a log file that outlines all actions of the
      // 'jacamar lock' process occurring in userspace. This should only be used for troubleshooting
      // potential errors with the process of generating/claiming file locks as there is no
      // automated cleanup on these files.
      file_lock_debug = false

      // RevalidateLock when enabled attempts to re-validate the lock file(s) associated with the
      // limited build directories before reporting. If any error is encountered a new lock file
      // will be pursued. This currently has limited testing and should likely be used in conjunction
      // with the FileLockDebug during initial deployment.
      revalidate_lock = false

  - .. note::

      The ``ff_limit_build_dir`` still remains a feature you must explicitly
      enable in your configuration and is subject to change based upon feedback.

Bug & Development Fixes
-----------------------

* Go version *1.22.2*
  (`!491 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/491>`_)
* Update linter versions and configurations
  (`!493 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/493>`_)
