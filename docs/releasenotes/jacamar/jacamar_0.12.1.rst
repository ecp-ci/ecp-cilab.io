Jacamar CI v0.12.1
==================

* *Release*: `v0.12.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.12.1>`_
* *Date*: 9/23/2022

Admin Changes
-------------

* Support ``auth.job_token_scope_enforced`` to verify
  `limited job token access <https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#limit-gitlab-cicd-job-token-access>`_
  at time of job authorization
  (`!380 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/380>`_)

  - Requires server version *15.4+*

Bug & Development Fixes
-----------------------

* Update gljobctx-go *v0.3.1* to support ``/-/jwks`` retry
  (`!387 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/387>`_)
