Jacamar CI v0.24.2
==================

* *Release*: `v0.24.2 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.24.2>`_
* *Date*: 2/13/2025

Admin Changes
-------------

* Offer Flux ``sacct`` replacement
  (`!566 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/566>`_)

  - .. code-block:: toml

      [batch]
        flux_sacct_replacement = true

  - This optional configuration uses ``flux jobs`` to identify if the
    job passed for the Slurm executor instead of ``sacct``.
