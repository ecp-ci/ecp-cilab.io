Jacamar CI v0.22.0
==================

* *Release*: `v0.22.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.25.0>`_
* *Date*: 9/6/2024

Admin Changes
-------------

* Dynamic scheduler arguments user script feature flag
  (`!519 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/519>`_)

  * .. code-block:: toml

      [batch]
        ff_dynamic_arg_script = true

  * This is a new feature we wish to further test before
    making baseline. It aims to allow users' flexibility
    in batch job submission not currently possible with CI variables alone.
    Simply referring to a script in the ``SCHEDULER_PARAMETERS`` will
    allow it to run and the expected JSON return will be parsed.

  * .. code-block:: shell

      #!/bin/bash

      # args - Equivalent to traditional SCHEDULER_PARAMETERS.
      # skip - Indicates if a job should be completed with exit status 0, without submitting a job script.

      echo "{\"args\":\"-N1 --account example --queue ci\",\"skip\":false}"

  * .. code-block:: yaml

      variables:
        SCHEDULER_PARAMETERS: "${CI_PROJECT_DIR}/scheduler.bash"

* Remove deprecated ``EnforceTokenScope``
  (`!516 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/516>`_)

  - The required API response was removed in GitLab server *17.0* and
    the feature can no longer be supported.

Bug & Development Fixes
-----------------------

* Expand RunMechanism to all user run stages
  (`!521 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/521>`_)
* Go version *1.22.5*
  (`!514 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/514>`_)
* Update dependencies
  (`!515 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/515>`_)
* Minor refactor to use of validator package
  (`!512 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/512>`_)
