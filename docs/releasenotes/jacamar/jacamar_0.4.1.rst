Jacamar CI v0.4.1
=================

* *Release*: `v0.4.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.4.1>`_
* *Date*: 02/16/2021

General Changes
---------------

* Added `CheckJWT` to the `rules` package
  (`!91 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/91>`_).

Admin Changes
-------------

* Support new enhanced broker registration requirements
  (`!93 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/93>`_).

  - Previous versions of Jacamar will no longer work with
    the broker service going forward.

* Update GitLab-Runner to version ``13.8.0``, with minimized
  patching requirements for testing purposes
  (`!90 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/90>`_).

Bug & Development Fixes
-----------------------

* Correctly remove ``CI_JOB_JWT`` from user environment only when federation or
  broker service is enabled
  (`!89 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/89>`_).

  - Previous iterations of Jacamar aggressively removed the JWT regardless
    of configurations.
