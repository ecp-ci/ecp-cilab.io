Jacamar CI v0.16.0
==================

* *Release*: `v0.16.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.16.0>`_
* *Date*: 6/9/2023

Admin Changes
-------------

* Pass user identities to RunAs validation script - (Thanks `@joe-snyder`)
  (`!433 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/433>`_)
* Correct log ``level`` configuration to match documentation
  (`!435 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/435>`_)

  - If you already had a functional logging level configuration there are
    no changes required.

  - .. code-block:: toml

      [logging]
      level = "debug"

Bug & Development Fixes
-----------------------

* Update keys and scripting for optional nightly runner testing
  (`!436 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/436>`_)
* Upgraded to Go version *1.20.5*
  (`!440 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/440>`_.
  `!444 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/444>`_)
* Remove support for fedora-builder image
  (`!439 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/439>`_)
