Jacamar CI v0.10.0
==================

* *Release*: `v0.10.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.10.0>`_
* *Date*: 1/31/2022

.. note::

    Release *v0.9.0* relocated all RPM installed binaries into a single
    location, ``/opt/jacamar/bin``. This will offer a better standard moving
    forward, please be aware of this when upgrading from an older version.

Admin Changes
-------------

* JSON Web Token package updates and configurable expiration delay
  (`!301 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/301>`_)

  - By default cleanup related actions will allow a 15-minute expiration
    in the JWT to account for a range of potential delays that conflict
    with the requirement to validate at every stage.

  - .. code-block:: toml

        [auth]
        jwt_exp_delay = "15m"

* Added option for unrestricted command line arguments
  (`!294 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/294>`_)

  - This feature is intended for deployments where ``hidepid``
    has been used when mounting
    `proc(5) <https://man7.org/linux/man-pages/man5/proc.5.html>`_.

  - .. code-block:: toml

        [general]
        unrestricted_cmd_line = true

* Improved default seccomp and added plugin feature flag support
  (`!286 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/286>`_)

  - By default when ``downscope = "setuid"`` any ``setuid`` or
    ``setgid`` target is restricted to the validated user.

  - .. code-block:: toml

        [auth.seccomp]
        ff_enable_plugin = true
        validation_plugin = "/opt/jacamar/plugins/seccomp.so"

  - .. note::

        The ``validation_plugin`` will remain behind the associated
        feature flag until more testing/feedback can be organized
        for the next release. Please be aware that this feature is
        subject to changes.

  - Go `plugin <https://pkg.go.dev/plugin>`_ support has been added
    to invoke a ``SeccompExpansion(*libseccomp.ScmpFilter, string) error``
    function during the startup of ``jacamar-auth``, providing the default
    set of filters for potential modification before they are loaded.

  - .. code-block:: go

        package main

        import (
            "syscall"
            libseccomp "github.com/seccomp/libseccomp-golang"
        )

        func SeccompExpansion(filter *libseccomp.ScmpFilter, stage string) error {
            callID, _ := libseccomp.GetSyscallFromName("mkdir")

            return filter.AddRule(callID, libseccomp.ActErrno.SetReturnCode(int16(syscall.EPERM)))
        }

* Explicitly manage signals in non-root cases for ``setuid`` with a constructed
  ``jacamar signal SIGNAL PID`` command
  (`!316 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/316>`_)

  - The ``jacamar signal`` command is only generated in cases where
    ``root`` permissions or capabilities (``CAP_KILL``) are lacking.
    This process observes the same limitations and protections as
    any other downscoped ``jacamar`` command.

  - Signals are observed in accordance with the
    `custom executor <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/316>`_,
    this means ``SIGTERM`` and ``SIGKILL`` respectively.

* Added system logging when ``jacamar-auth`` encounters
  `SIGTERM <https://www.man7.org/linux/man-pages/man7/signal.7.html>`_
  (`!305 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/305>`_)

* Process timeout established for ``jacamar`` and ``jacamar-auth``
  based upon job timeout
  (`!311 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/311>`_)

  - This timeout context is meant to address potential cases where the
    parent process (e.g., ``gitlab-runner``) is no longer able to send
    the appropriate signal.

Bug & Development Fixes
-----------------------

* Error handling enhancements for internal API
  (`!302 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/302>`_)
* GitLab `license scanning <https://gitlab.com/ecp-ci/jacamar-ci/-/licenses#licenses>`_
  to verify compliance
  (`!298 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/298>`_)
* Minimize argument requirements while ensuring existing support
  (`!288 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/288>`_)
* Introduce ``Signaler`` interface to command structure
  (`!313 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/313>`_)
* Remove watcher package in favor of more minimal system packages
  (`!300 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/300>`_)
* Improve Pavilion test result output
  (`!299 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/299>`_)
* Updated all build tag formats for Go *1.17*
  (`!293 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/293>`_)
* Clarified function name and documentation in ``verifycaps`` package
  (`!287 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/287>`_)
* Added basic tests for NERSC instance
  (`!292 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/292>`_)
* Corrected ``VERSION`` being provided during container builds
  (`!321 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/321>`_)

  - .. code-block:: shell

        $ make rpm-container VERSION=0.10.0.example
        Running RPM Build in podman...
        ...
        /builds/ecp-ci/jacamar-ci/rpms/jacamar-ci-0.10.0.example-1.el7.x86_64.rpm

* Correct Slurm test image and test syntax
  (`!319 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/319>`_)
* Verify ``go.mod`` changes are committed during CI
  (`!318 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/318>`_)
* Mock ``/jobs`` endpoint fork Pavilion test scripts
  (`!317 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/317>`_)
