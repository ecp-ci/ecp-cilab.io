Jacamar CI v0.19.0
==================

.. _Deploying and Using the Podman Run Mechanism: ../../guides/deploy-use-podman-mechanism.html

* *Release*: `v0.19.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.19.0>`_
* *Date*: 4/4/2024

.. note::

  Due to a mistake in preparing the release I left out a fix
  for handling error messages associated with ``podman pull``. Please use
  the *v0.19.1* release instead.

Admin Changes
-------------

* Introduce Podman run mechanism
  (`!419 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/419>`_,
  `!484 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/484>`_,
  `!483 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/483>`_)

  - Details can be found in the
    `Deploying and Using the Podman Run Mechanism`_ guide.

* Support `enforce_nologin` configuration during auth
  (`!480 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/480>`_)

  - .. code-block:: toml

      [auth]
        enforce_nologin = true

  - Enabling this indicates that jobs should be blocked during configuration
    if a *pam_nologin* file (``/etc/nologin`` or ``/var/run/nologin``)
    are encountered.
