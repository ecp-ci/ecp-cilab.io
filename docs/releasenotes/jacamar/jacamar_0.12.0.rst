Jacamar CI v0.12.0
==================

* *Release*: `v0.12.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.12.0>`_
* *Date*: 8/26/2022

Admin Changes
-------------

* Updated seccomp support and logging
  (`!360 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/360>`_)

  - Details can be found in the guide:
    `Configuring and Troubleshooting Seccomp <https://ecp-ci.gitlab.io/docs/guides/seccomp-jacamar-administration.html>`_

* Make *ioctl* rule optional and prevent invalid setuid deployments
  (`!351 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/351>`_)
* Increase default ``kill_timeout`` to 2 minutes
  (`!376 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/376>`_)
* Optional static build directory configuration
  (`!361 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/361>`_)
* ``make release-binaries`` for binary only RPM
  (`!369 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/369>`_)

  - .. code-block:: bash

        $ rpm -qlip jacamar-ci-binaries-0.11.0.pre.eeb5d98.el7.x86_64.rpm
        Name        : jacamar-ci-binaries
        Version     : 0.12.0
        ...
        Relocations : /usr
        Summary     : HPC focused CI/CD driver for the GitLab custom executor binary only
        ...
        /usr/bin/jacamar
        /usr/bin/jacamar-auth

* Add current executable path to ``jacamar`` command creation
  (`!371 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/371>`_)

  - This will allow for cases where the ``jacamar_path`` is not configured and
    construct the fully qualified path for the ``jacamar`` application to
    allign with the location of ``jacamar-auth``.

* Log restricted privileges that conflict with downscoping
  (`!356 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/356>`_)

Bug & Development Fixes
-----------------------

* Establish testing structure for latest runner compatability
  (`!363 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/363>`_,
  `!354 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/354>`_)
* Correct PBS signal handling and expand logging/testing
  (`!367 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/367>`_)

  - As part of this fix debug logs have been introduced to the job
    specific script directory that will be removed during cleanup
    if logs are not copied/retained through existing mechanisms.

  - .. code-block:: bash

        $ cat $HOME/.jacamar-ci/scripts/abcd1234/000/group/project/123/sigterm.log
        Signal captured, attempting to cancel job (scancel 4)
        command error: ...

* Handle response during ``--no-auth`` deployment
  (`!373 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/373>`_)
* Allow for limited buffer ``ReturnOutput`` via downscoped command
  (`!370 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/370>`_)
* Support for obtaining software bill of materials
  (`!366 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/366>`_)

  - Constructed using
    `cyclonedx-gomod <https://github.com/CycloneDX/cyclonedx-gomod>`_.

* Update CentOS 7 builder image
  (`!362 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/362>`_)
* Tool for seccomp configuration testing
  (`!358 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/358>`_)

  - See the `README.md <https://gitlab.com/ecp-ci/jacamar-ci/-/tree/develop/tools/seccomp-tester>`_
    for details.
