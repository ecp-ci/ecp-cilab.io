Jacamar CI v0.19.1
==================

* *Release*: `v0.19.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.19.1>`_
* *Date*: 4/5/2024

Admin Changes
-------------

* Correct Podman pull output
  (`!488 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/488>`_)
