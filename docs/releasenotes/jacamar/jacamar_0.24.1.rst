Jacamar CI v0.24.1
==================

* *Release*: `v0.24.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.24.1>`_
* *Date*: 12/22/2024

Admin Changes
-------------

* Support feature for ``user_bash_env`` configuration
  (`!556 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/556>`_)

  - This optional configuration injects ``key=value`` pairs into the
    shell generated for a user to launch job scripts and related
    monitoring commands. An example use case could involve overriding
    the LMOD administrative file, thus preventing the output from
    appearing multiple times in every CI job log:

  - .. code-block:: toml

      [batch]
        user_bash_env = ["LMOD_ADMIN_FILE='none'"]

Bug & Development Fixes
-----------------------

* Capture scheduler interactions via file
  (`!554 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/554>`_)

  - This changes improves scheduler based executor (primarily Flux and Slurm)
    by improving how output is parsed.

* Log target scheduler action
  (`!555 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/555>`_)
* Fix runner integration testing
  (`!553 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/553>`_)

  - This corrects errors we've seen when deploying the nightly build of
    the GitLab Runner used for integration testing. All failures where
    unrelated to the application itself.

* Correct Slurm ``allocate`` action monitoring
  (`!558 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/558>`_)
