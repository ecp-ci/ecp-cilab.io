Jacamar CI v0.24.0
==================

* *Release*: `v0.24.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.24.0>`_
* *Date*: 12/10/2024

User Changes
------------

* Support user defined ``SCHEDULER_JOB_PREFIX``
  (`!546 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/546>`_)

  - .. code-block:: yaml

      job:
        variables:
          SCHEDULER_JOB_PREFIX: "jacamar-dev"
          SCHEDULER_PARAMETERS: "-N1"

  - The resulting job will have now be submitted with the
    ``--job-name=jacamar-dev_ci-2287240``, the post fixed
    *CI_JOB_ID* is required. Please note that your defined
    name is limited to alphanumeric, underscores, and hyphen
    characters restricted to 4-64 characters.

Admin Changes
-------------

* Prevent usage of ``SCHEDULER_JOB_PREFIX`` variable
  (`!546 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/546>`_)

  - By default this feature has been enabled for all users; however,
    it can be disabled via runner configuration if required.

  - .. code-block:: toml

      [batch]
        disable_name_prefix = true

Bug & Development Fixes
-----------------------

* Fix nightly integration testing
  (`!542 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/542>`_)
* Introduce ``execerr`` package to help identify build errors
  (`!539 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/539>`_)

  - Please note that until upstream efforts are accepted this and all
    other changes relating to build exit code errors are only laying
    the groundwork for any finalized support.

* Improve exit code identification for Flux and Slurm
  (`!532 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/532>`_,
  `!544 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/544>`_)
* Upgrade ``govulncheck``
  (`!549 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/549>`_)
* Go version *1.23.3*
  (`!547 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/547>`_)
* Local Slurm integration testing ``make pav-local-slurm``
  (`!541 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/541>`_)
