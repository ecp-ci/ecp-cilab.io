Jacamar CI v0.17.1
==================

* *Release*: `v0.17.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.17.1>`_
* *Date*: 1/19/2024

User Changes
------------

* Optionally use ``GIT_CONFIG_*`` variables for skipping ``credential.helper``
  (`!469 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/469>`_)

  - Previous versions of Jacamar CI directly modified the GitLab generated job
    script to realize this; however, to better ensure reliability moving forward
    we've shifted to relying on setting the related environment variables.

  - .. code-block:: shell

        export GIT_CONFIG_COUNT='1'
        export GIT_CONFIG_KEY_0='credential.helper'
        export GIT_CONFIG_VALUE_0=''

  - This change requires the use of Git version 2.31+ in order to function,
    however, it is not required and jobs without a defined
    ``credential.helper`` will continue to function.

* Modify ``GIT_ASKPASS`` script to improve reliability
  (`!468 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/468>`_)

  - Better aligns with how
    `Git retrieves credentials <https://github.com/git/git/blob/master/credential.c#L201>`_
    to support cases where the username and password have to be supplied. This
    should help address issue individuals may have experienced when dealing
    with Git submodules.
