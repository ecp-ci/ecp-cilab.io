Jacamar CI v0.25.0
==================

.. _Charliecloud: https://hpc.github.io/charliecloud/
.. _Deploying and Using the Charliecloud Run Mechanism: ../../guides/deploy-use-charliecloud-mechanism.html

* *Release*: `v0.25.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.25.0>`_
* *Date*: 2/24/2025

Admin Changes
-------------

* Introduced Charliecloud_ run mechanism
  (`!518 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/518>`_)

  - Details can be found in the
    `Deploying and Using the Charliecloud Run Mechanism`_ guide.

* New capability to report build exit code
  (`!562 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/562>`_,
  `!570 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/570>`_,
  `!561 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/561>`_)

  - Thanks to changes accepted into the GitLab Runner
    (`MR 5028 <https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/5028>`_)
    it is now possible to report the true build exit code. This means
    features such as the
    `allow_failure:exit_codes <https://docs.gitlab.com/ci/yaml/#allow_failureexit_codes>`_
    will now function correctly.

  - This feature is supported across all executors (with the exception of
    Cobalt) and only requires that GitLab runner *v17.9+* is installed.

  - If issues arise with jobs incorrectly identifying the exit code please first
    attempt enabling the ``FF_USE_NEW_BASH_EVAL_STRATEGY``
    `runner feature flag <https://docs.gitlab.com/runner/configuration/feature-flags/>`_.
    If this does not help we encourage you to
    `open an issue <https://gitlab.com/ecp-ci/jacamar-ci/-/issues/new?issuable_template=Bug>`_.

* Optional ``skip_exit_code_file`` configuration
  (`!569 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/569>`_)

  - If unforeseen issue arise with exit code identification if can be
    disabled via configuration. This will revert Jacamar CI behavior
    back to using the generic
    `Build Failure <https://docs.gitlab.com/runner/executors/custom/#build-failure>`_.

  - .. code-block:: toml

      [auth]
        skip_exit_code_file = true

* Added new ``batch.allowed_actions`` configuration to limit potential
  scheduler actions
  (`!567 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/567>`_)

  - Refer to the `Scheduler Actions <https://ecp-ci.gitlab.io/docs/guides/scheduler-actions.html#utilizing-feature-in-ci-cd-pipelines>`_
    documentation for the full list of supported actions.

  - .. code-block:: toml

      [batch]
        scheduler_actions = true
        allowed_actions =  ["detach", "cancel"]

Bug & Development Fixes
-----------------------

* Correctly capture PBS exit code
  (`!565 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/565>`_)
* Refactor ``runmechanism`` to limit duplications
  (`!560 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/560>`_)
* Go version *1.23.5*
  (`!563 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/563>`_)
* New internal package to organize executor types
  (`!570 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/570>`_)
