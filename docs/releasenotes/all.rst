:orphan:

All Releases
============

Detailed release notes for all runner and server development efforts.

.. toctree::
   :maxdepth: 1
   :caption: Jacamar CI

   jacamar/jacamar_0.25.0.rst
   jacamar/jacamar_0.24.2.rst
   jacamar/jacamar_0.24.1.rst
   jacamar/jacamar_0.24.0.rst
   jacamar/jacamar_0.23.0.rst
   jacamar/jacamar_0.22.0.rst
   jacamar/jacamar_0.21.0.rst
   jacamar/jacamar_0.20.0.rst
   jacamar/jacamar_0.19.2.rst
   jacamar/jacamar_0.19.1.rst
   jacamar/jacamar_0.19.0.rst
   jacamar/jacamar_0.18.0.rst
   jacamar/jacamar_0.17.1.rst
   jacamar/jacamar_0.17.0.rst
   jacamar/jacamar_0.16.0.rst
   jacamar/jacamar_0.15.0.rst
   jacamar/jacamar_0.14.0.rst
   jacamar/jacamar_0.13.0.rst
   jacamar/jacamar_0.12.1.rst
   jacamar/jacamar_0.12.0.rst
   jacamar/jacamar_0.11.0.rst
   jacamar/jacamar_0.10.2.rst
   jacamar/jacamar_0.10.1.rst
   jacamar/jacamar_0.10.0.rst
   jacamar/jacamar_0.9.1.rst
   jacamar/jacamar_0.9.0.rst
   jacamar/jacamar_0.8.2.rst
   jacamar/jacamar_0.8.1.rst
   jacamar/jacamar_0.8.0.rst
   jacamar/jacamar_0.7.3.rst
   jacamar/jacamar_0.7.2.rst
   jacamar/jacamar_0.7.1.rst
   jacamar/jacamar_0.7.0.rst
   jacamar/jacamar_0.6.0.rst
   jacamar/jacamar_0.5.0.rst
   jacamar/jacamar_0.4.2.rst
   jacamar/jacamar_0.4.1.rst
   jacamar/jacamar_0.4.0.rst
   jacamar/jacamar_0.3.2.rst
   jacamar/jacamar_0.3.1.rst
   jacamar/jacamar_0.3.0.rst
   jacamar/jacamar_0.2.0.rst
   jacamar/jacamar_0.1.0.rst
