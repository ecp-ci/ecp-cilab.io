#!/usr/bin/env python3
# coding=utf-8

import datetime
import gitlab
import os
import statistics

# Set associated environment variables before running script.
url = os.getenv('GITLAB_SERVER_URL')
project_id = os.getenv('GITLAB_PROJECT_ID')
private_token=os.getenv('GITLAB_TOKEN')
tag=os.getenv('GITLAB_RUNNER_TAG')


gl = gitlab.Gitlab(url=url, private_token=private_token)
proj = gl.projects.get(project_id)

print("Examining job utilization and time waiting for project '{0}' on {1}".format(
    proj.name_with_namespace, url))

tar_end = datetime.datetime.now() - datetime.timedelta(days=7)
jobs = proj.jobs.list(iterator=True)

queued_durations = []

for job in jobs:
    if datetime.datetime.strptime(job.created_at.split('T')[0], '%Y-%m-%d') < tar_end:
        break
    if tag in job.tag_list:
        if job.queued_duration is None:
            queued_durations.append(0)
        else:
            queued_durations.append(job.queued_duration)

print("\tTotal jobs: {}".format(len(queued_durations)))
print("\tMean: {}s".format(round(statistics.mean(queued_durations))))
print("\tMedian: {}s".format(round(statistics.median(queued_durations))))
