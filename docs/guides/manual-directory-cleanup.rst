Manual Directory Cleanup
========================

The goal of this example is to demonstrate a potential workflow that will
provide teams with the ability to `manually <https://docs.gitlab.com/ee/ci/yaml/#when>`_
run cleanup tasks. This may be a desirable workflow for teams that generate
substantial data and use underlying file systems.

.. important::

    Removing files from a system as part of your CI job can be dangerous.
    Take care to verify the target directories are properly defined to
    avoid automatically removing required directories.

Example ``.gitlab-ci.yml``
--------------------------

.. literalinclude:: ../../source/manual-directory-cleanup/.gitlab-ci.yml
    :language: yaml

Pipeline Results
----------------

.. image:: files/manual-remove-generic.png
    :scale: 60%

Notes
-----

* There are other GitLab supported tools you can leverage in such workflows.
  We recommend reading: `protecting manual jobs <https://about.gitlab.com/blog/2020/02/20/protecting-manual-jobs/>`_.
* Make cleanup a job-level action by creating an `after_script <https://docs.gitlab.com/ee/ci/yaml/#after_script>`_;
  however, it should be noted that canceled jobs end abruptly preventing the
  ``after_script`` from running. Please refer to official GitLab efforts to
  `ensure after_script is called for cancelled and timed out pipelines <https://gitlab.com/gitlab-org/gitlab/-/issues/15603>`_
  for the status of a fix.
