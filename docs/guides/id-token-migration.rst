Migrating to new id_tokens from CI_JOB_JWT
==========================================

.. _extends: https://docs.gitlab.com/ee/ci/yaml/#extends
.. _include: https://docs.gitlab.com/ee/ci/yaml/#include
.. _keywords: https://docs.gitlab.com/ee/ci/yaml/
.. _id_tokens: https://docs.gitlab.com/ee/ci/yaml/index.html#id_tokens

.. important::

    Once you deploy server version *17.0* releases of Jacamar CI
    prior to *v0.18.0* will no longer function.

With the release of
`GitLab server 17.0 <https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/>`_
there are several keys JWT related features being
deprecated that you should be aware of:

* `Old versions of the JWT are being fully deprecated <https://docs.gitlab.com/ee/update/deprecations.html#old-versions-of-json-web-tokens-are-deprecated>`_
  in favor of id_tokens_. If you are un-familiar with the ``CI_JOB_JWT`` in
  Jacamar CI, it is utilized to consistently and securely identify key
  information about the CI job and most importantly the user responsible
  for triggering it.

  * For additional details regarding the changes and the new ``id_tokens``
    please see the official
    `Secure GitLab CI/CD workflows using OIDC JWT on a DevSecOps platform <https://about.gitlab.com/blog/2023/02/28/oidc/>`_
    blog post. It is an excellent resource and if you're unfamiliar
    with JSON Web Tokens in GitLab it can help immensely.

* `JWKS instance endpoint <https://docs.gitlab.com/ee/update/deprecations.html#jwt--jwks-instance-endpoint-is-deprecated>`_
  (``/-/jwks``) is being removed in favor of the
  ``/oauth/discovery/keys``. Upgrading to Jacamar CI is the only
  fix for this.

Migrating with Jacamar CI v0.18+
--------------------------------

Before proceeding with the migration verify that your server is *v15.7+*.
Previous versions will not have the id_tokens_ functionality available.

Install the *v0.18+* `release <https://gitlab.com/ecp-ci/jacamar-ci/-/releases>`_
of Jacamar CI on your system. Then you'll need to modify your Jacamar CI
`configuration <../admin/jacamar/configuration.html#general-table>`_
to include:

.. code-block:: toml

    [general]
    jwt_env_variable = "SITE_ID_TOKEN"

.. warning::

    The ``jwt_env_variable`` defines the expected environment variable Jacamar CI
    will look for, if not present the default ``CI_JOB_JWT`` will be used.
    The value can be anything you want; however, we advise not using
    ``CI_JOB_JWT`` else Jacamar CI cannot tell the difference and will be unable to
    warn the user correctly of any required job modification.

With just that modification anyone utilizing this runner will now encounter
the following warning if a job does not contain the defined
``jwt_env_variable``:

.. code-block:: shell

    Preparing the "custom" executor
    No id_token found on SITE_ID_TOKEN variable. Please update your CI job to include the following:

      id_tokens:
        SITE_ID_TOKEN:
          aud: https://gitlab.example.com

    Prior to server release v16.0 the default CI_JOB_JWT will remain available. For additional details see: https://docs.gitlab.com/ee/ci/yaml/index.html#id_tokens
    Using Custom executor with driver Jacamar CI 0.18.0...

Once the server is upgraded to *v17.0+* the ``CI_JOB_JWT`` will no longer
be provided by default and the job will fail with an obfuscated authorization
error; however, this warning message will remain. Please keep in mind that even
though users can request id_tokens_ they remain signed by the GitLab server and
will be strictly verified against the public keys.

Once the new id_tokens_ field has been added the warning message will
no longer appear. You can further verify the desired functionality by
`limiting JWT access <https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html#enable-automatic-id-token-authentication>`_
in the project settings which completely removes the default ``CI_JOB_JWT``
from any and all jobs.

Leveraging Include/Extends
--------------------------

There are ways that you can assist users in migrating jobs, while at
the same time provide a degree of future proofing with:

* include_ - "Import configuration from other YAML files"
* extends_ - "Configuration entries that this job inherits from"

The goal is to establish a documented practice that encourages the
use of administratively defined configurations users can import
as opposed to defining specific ``tags``/``id_tokens``.

For example, let's say we have a project called ``defaults`` that exists
within our ``ci/`` sub-group that is made available to users as
an `internal project <https://docs.gitlab.com/ee/user/public_access.html>`_
with maintenance/development permissions restricted to appropriate
administrators. Here we can create a ``runners.yml`` file for
the purpose of providing runner specific configurations to our CI users:

.. code-block:: yaml

    .site-id-token: &site-id-token
      id_tokens:
        CI_JOB_JWT:
          aud: https://gitlab.example.com

    .example-shell-runner:
      <<: *site-id-token
      tags:
        - example-system
        - shell

    .example-slurm-runner:
      <<: *site-id-token
      tags:
        - example-system
        - slurm

    .dev-shell-runner:
      <<: *site-id-token
      tags:
        - dev-system
        - shell

Now when migrating a ``.gitlab-ci.yml`` to prepare for the *v17.0+*
server release it would look like:

.. code-block:: yaml

    include:
      - project: 'ci/defaults'
        ref: main
        file:
          - '/runners.yml'

    build:
      extends:
        # The tags/id_token will be inherited as defined in our runners.yml file.
        # Any previously existing tags should be removed.
        - .example-shell-runner
      script:
        - make build

.. note::

    Any keywords_ established in our default configurations can be overwritten
    by users at the job level.

Users will still be responsible for updating any and all ``.gitlab-ci.yml``
files to leverage these new configurations. However, by choosing to use this
include_ + extends_ workflow we hope that any future requirements or runner
modifications become easier to realize without the need for manual
intervention.

Beginning Migration with Earlier Jacamar CI Versions
----------------------------------------------------

.. note::

  Though it is is possible to begin migration to id_tokens_ with this method
  you will need to upgrade to Jacamar CI to *v0.18+* in order to correctly
  handle other feature being deprecated in the server.

You are not required to upgrade your deployment of Jacamar CI to **begin**
migrating to id_tokens_, only that the server is *v15.7+*.
Simply have users add the following to any CI job using Jacamar CI:

.. code-block:: yaml

    example_job:
      id_tokens:
        CI_JOB_JWT:
          aud: https://gitlab.example.com

To preserve compatibility Jacamar CI will always attempt to check
the ``CI_JOB_JWT`` variable for a valid token. The audience (``aud``)
can be set to anything you want, though we recommend using a
consistent value as you can later enforce a required audience
via configuration.

Another option
`introduced in GitLab server v16.4 <https://gitlab.com/gitlab-org/gitlab/-/issues/419750>`_
is defining id_tokens_ at the ``default`` level:

.. code-block:: yaml

    default:
      id_tokens:
        CI_JOB_JWT:
          aud: https://gitlab.example.com

Doing this will cause all jobs within your pipeline to to have
the ``CI_JOB_JWT`` generated and included automatically. For
projects exclusively using Jacamar CI this will likely present
the easiest migration option.
