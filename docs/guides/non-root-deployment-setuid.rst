Non-Root Jacamar CI Downscoping (with Capabilities) via SetUID
==============================================================

Jacamar CI offers a robust series of configurations that seek to support
advanced CI/CD workflows on HPC test resources. As part of these workflows
the authorization process enables optional
`downscoping <../admin/jacamar/auth.html#downscoping-mechanisms>`_
that limits permissions to that of the pipeline trigger user's local account.
Traditionally this would require the usage of `root`; however, with
recent improvements we now support downscoping by leveraging Linux
`capabilities <https://man7.org/linux/man-pages/man7/capabilities.7.html>`_.
This guide details how you can potentially deploy, configure, and run Jacamar
CI in such a manner.

.. note::

    If you've never used Jacamar CI or the GitLab Custom Executor before we
    highly encourage you to first explore the
    `Administrative Tutorial <../admin/jacamar/tutorial.html>`_.

Deployment
----------

To begin the deployment process you will require usage of a privileged
account.

1. Obtain the latest
   `Jacamar CI RPMs <https://gitlab.com/ecp-ci/jacamar-ci/-/releases>`_
   and `runner release <https://docs.gitlab.com/runner/install/linux-manually.html#download>`_.

  - With Jacamar CI `release packages <../admin/jacamar/deployment.html#packages>`_
    there is an optional deployment with configuration already enabled for use
    with the ``gitlab-runner`` account ("RPM - w/Capabilities").
    This guide details configuring both.

2. Install both RPMs and any necessary dependencies (``sudo rpm -i ...``).

.. note::

    Release *v0.9.0* relocated all RPM installed binaries into a single
    location, ``/opt/jacamar/bin``. This will offer a better standard moving
    forward, please be aware of this when upgrading from an older version.

3. Switch to the non-root user account (e.g., ``gitlab-runner``) and
   register the runner with your target GitLab server:

  - ``gitlab-runner register``

  - There are `non-interactive <https://docs.gitlab.com/runner/register/#one-line-registration-command>`_
    options as well external tools
    (e.g., `LLNL/gitlab-runner-auth <https://github.com/LLNL/gitlab-runner-auth>`_)
    that can be used to manage the registration process.

.. important::

    You will need a service account that will take responsibility for both the
    ``gitlab-runner`` and ``jacamar-auth`` processes as well as
    configurations. For the purposes of this guide we are using a
    ``gitlab-runner`` user account, if you've deployed the runner with the
    `official release <https://docs.gitlab.com/runner/install/>`_ this
    service account likely already exists.

Configurations
--------------

Configure both the `Runner`_ and `Jacamar CI`_ in
a mostly traditional manner; however, special care must be given
to the location these files are stored. Our ``gitlab-runner`` user
must be able to read the files and sensitive tokens/configurations
can be found in both. To accomplish this we are going to keep
both configuration in the user's home directory
(``/home/gitlab-runner/.gitlab-runner``) but you are free
to store wherever easiest.

Runner
~~~~~~

1. Edit ``/home/gitlab-runner/.gitlab-runner/config.toml``
   generated during registration to add the appropriate
   options to the ``[runners.custom]`` table:

    - .. code-block:: toml

        concurrent = 5
        check_interval = 0

        [session_server]
          session_timeout = 1800

        [[runners]]
          name = "Jacamar CI Cap Testing"
          url = "https://gitlab.example.com/"
          token = "<RUNNER-TOKEN>"
          executor = "custom"
          [runners.custom]
            config_exec = "/opt/jacamar/bin/jacamar-auth"
            config_args = ["config", "--configuration", "/home/gitlab-runner/.gitlab-runner/jacamar-config.toml"]
            prepare_exec = "/opt/jacamar/bin/jacamar-auth"
            prepare_args = ["prepare"]
            run_exec = "/opt/jacamar/bin/jacamar-auth"
            run_args = ["run"]
            cleanup_exec = "/opt/jacamar/bin/jacamar-auth"
            cleanup_args = ["cleanup", "--configuration", "/home/gitlab-runner/.gitlab-runner/jacamar-config.toml"]

Jacamar CI
~~~~~~~~~~

1. Create the ``/home/gitlab-runner/.gitlab-runner/jacamar-config.toml``
   we referenced in our `Runner`_ configuration:

  - .. code-block:: toml

      [general]
        executor = "shell"
        data_dir = "$HOME"

      [auth]
        downscope = "setuid"

Please note we've demonstrated a very minimal configuration for the purposes
of managing jobs under a non-root user. Please reference Jacamar CI's
`configuration documentation <../admin/jacamar/configuration.html>`_
for a complete list of potential options.

System Service
--------------

Returning to a privileged account:

1. Identify if the default runner process is installed,
   ``sudo systemctl status gitlab-runner``

  - If missing use the runner to install a
    `system service <https://docs.gitlab.com/runner/configuration/init.html>`_.

  - **Important**, avoid having multiple runner services active on the same
    machine, this can lead to unexpected results.

.. tab-set::

    .. tab-item:: Installed RPM w/Capabilities

        2. Edit the ``/etc/systemd/system/gitlab-runner.service`` file:

          - .. code-block:: shell

                [Unit]
                Description=GitLab Runner using Jacamar CI with Capabilities
                After=syslog.target network.target
                ConditionFileIsExecutable=/usr/bin/gitlab-runner

                [Service]
                StartLimitInterval=5
                StartLimitBurst=10
                ExecStart=/usr/bin/gitlab-runner "run" "--syslog"  "--working-directory" "/opt/jacamar" "--config" "/home/gitlab-runner/.gitlab-runner/config.toml" "--service" "gitlab--runner" "--user" "gitlab-runner"
                Restart=always
                RestartSec=120
                User=gitlab-runner
                Group=gitlab-runner

                [Install]
                WantedBy=multi-user.target

          - It is important to target the ``gitlab-runner`` account (using ``User``
            and ``Group``) as both the ``gitlab-runner`` and ``jacamar-auth``
            applications will execute under the same user.

          - .. code-block:: shell

                $ getcap /opt/jacamar/bin/jacamar-auth
                /opt/jacamar/bin/jacamar-auth cap_setgid,cap_setuid=ep


    .. tab-item:: Installed Standard Release

        2. Edit the ``/etc/systemd/system/gitlab-runner.service`` file:

          - .. code-block:: shell

                [Unit]
                Description=GitLab Runner using Jacamar CI with Capabilities
                After=syslog.target network.target
                ConditionFileIsExecutable=/usr/bin/gitlab-runner

                [Service]
                StartLimitInterval=5
                StartLimitBurst=10
                ExecStartPre=+bash -c '/usr/bin/chown gitlab-runner:gitlab-runner /opt/jacamar/bin/jacamar-auth && /usr/bin/chmod 700 /opt/jacamar/bin/jacamar-auth && /usr/sbin/setcap cap_setuid,cap_setgid=ep /opt/jacamar/bin/jacamar-auth'
                ExecStart=/usr/bin/gitlab-runner "run" "--syslog"  "--working-directory" "/opt/jacamar" "--config" "/home/gitlab-runner/.gitlab-runner/config.toml" "--service" "gitlab--runner" "--user" "gitlab-runner"
                Restart=always
                RestartSec=120
                User=gitlab-runner
                Group=gitlab-runner

                [Install]
                WantedBy=multi-user.target

          - It is important to target the ``gitlab-runner`` account (using ``User``
            and ``Group``) as both the ``gitlab-runner`` and ``jacamar-auth``
            applications will execute under the same user.

          - By declaring the ``=+`` in our ``ExecStartPre`` it will run the subsequent
            command as ``root`` as opposed to the ``gitlab-runner`` user. This allows for
            privileged actions (e.g., setting file ownership and capabilities_).

        .. note::

            If you are running a versions of `systemd <https://github.com/systemd/systemd/releases>`_
            older than *240* you will need to use the ``PermissionsStartOnly`` flag as opposed
            to ``=+``

           .. code-block::

                [Service]
                ...
                PermissionsStartOnly=true
                ExecStartPre=bash -c '...


3. Restart and verify service is running:

  - .. code-block:: shell

        sudo systemctl disable gitlab-runner.service
        sudo systemctl enable gitlab-runner.service
        sudo systemctl restart gitlab-runner.service
        sudo systemctl status gitlab-runner.service

Examine Process
---------------

Finally, with the runner polling for jobs we advise creating test cases
(``.gitlab-ci.yml``)to verify desired functionality.

.. code-block::

    ci-job:
      script:
        - id
        - /sbin/capsh --print
        - sleep 120

Our example job is just a simple look that demonstrates the user executing
the CI job locally, ensuring the capabilities were not improperly
set (inherited), and providing sufficient time during which you can more
closely examine the locally running processes:

.. code-block:: console

    $ ps -aef --forest
    UID          PID    PPID  C STIME TTY          TIME CMD
    ...
    gitlab-+    1053       1  0 15:40 ?        00:00:06 /usr/bin/gitlab-runner run --syslog --working-directory /opt/jacamar --config /home/gitlab-runner/.gitlab-runner/config.toml
    gitlab-+    2517    1053  0 17:20 ?        00:00:00  \_ /opt/jacamar/bin/jacamar-auth -u run /tmp/custom-executor440226574/script347499535/script. build_script
    usr         2523    2517  0 17:20 ?        00:00:00      \_ /usr/bin/jacamar --no-auth run env-script build_script
    usr         2528    2523  0 17:20 ?        00:00:00          \_ /usr/bin/bash --login
    usr         2553    2528  0 17:20 ?        00:00:00              \_ bash /home/usr/.jacamar-ci/scripts/short/000/group/project/981625/build_script.bash
    usr         2555    2553  0 17:20 ?        00:00:00                  \_ bash /home/usr/.jacamar-ci/scripts/short/000/group/project/981625/build_script.bash
    usr         2559    2555  0 17:20 ?        00:00:00                      \_ sleep 120

We encourage you to take whatever steps you feel sufficient to
ensure your deployment meets expectations beyond even what we show here.
However, at this point you are able to expand utilization or explore
additional `configuration options <../admin/jacamar/configuration.html>`_.
